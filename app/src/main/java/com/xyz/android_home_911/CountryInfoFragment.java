package com.xyz.android_home_911;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.xyz.android_home_911.R.id.imageView;


public class CountryInfoFragment extends Fragment {
    Country countryInfo;
    String country;

    OnDataPass onDataPass;

    @BindView(R.id.name_country)
    TextView countryName;

    @BindView(R.id.capital)
    TextView countryCapital;

    @BindView(R.id.region)
    TextView countryRegion;

    @BindView(R.id.numericCode)
    TextView countryNumericCode;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnDataPass) {
            onDataPass = (OnDataPass) context;
        } else {
            throw new RuntimeException("Must be MainActivity");
        }
    }

    @BindView(R.id.currencies)
    TextView countryCurrencies;

    @BindView(R.id.languages)
    TextView countryLanguages;


    @BindView(imageView)
    ImageView countryImageView;


    public void setCountry(String country) {
        this.country = country;
    }

    public static CountryInfoFragment newInstance() {
        return new CountryInfoFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_info, container, false);
        ButterKnife.bind(this, view);


        List<Country> countries = onDataPass.getData();

        Country countryData = new Country();
        for (int i = 0; i < countries.size(); i++) {
            String temp = countries.get(i).name.toString();
            if ((temp != "") & (temp == country)) {
                countryData = countries.get(i);
            }

        }

        updateData(countryData);

        if (countryInfo != null) {
            setData(countryInfo);
        }

        return view;
    }

    public void updateData(Country country) {
        this.countryInfo = country;
        if (isResumed()) {
            setData(country);
        }
    }


    public void setData(Country country) {

        if (country != null) {

            if (countryName != null) countryName.setText("Country name: " + country.name);
            if (countryCapital != null)
                countryCapital.setText("Country capital: " + country.capital);
            if (countryRegion != null)
                countryRegion.setText("Country region: " + country.region);
            if (countryNumericCode != null)
                countryNumericCode.setText("Country numeric code: " + country.numericCode);

            if (countryCurrencies != null) {
                countryCurrencies.setText("Country currencies: ");
                for (int i = 0; i < country.currencies.length; i++) {
                    countryCurrencies.append("\n" + (i + 1) + "-  " + country.currencies[i].name.toString());
                }
            }

            if (countryLanguages != null) {
                countryLanguages.setText("Country languages: ");
                for (int i = 0; i < country.languages.length; i++) {
                    countryLanguages.append("\n" + (i + 1) + "-  " + country.languages[i].name.toString());
                }
            }

            if (countryImageView != null) {

                Bitmap bmImg = BitmapFactory.decodeFile(country.flagImageURL);
                countryImageView.setImageBitmap(bmImg);

            }
        }

    }
}