package com.xyz.android_home_911;

/**
 * Created by user on 01.02.2018.
 */

public interface RegionElementClickListener {
    void onRegionItemClick(String item);
}
