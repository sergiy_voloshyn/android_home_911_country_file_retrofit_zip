package com.xyz.android_home_911;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Path;


public class Retrofit {

    private static final String ENDPOINT = "http://restcountries.eu/rest";
    private static ApiInterface apiInterface;

    static {
        initialize();
    }


    interface ApiInterface {

        //https://restcountries.eu/rest/v2/all?
        @GET("/v2/all")
        void getAllInformation(Callback<List<Country>> callback);

        //https://restcountries.eu/rest/v2/all?fields=region
        @GET("/v2/all?fields=region")
        void getRegions(Callback<List<Country>> callback);

        //https://restcountries.eu/rest/v2/subregion/Caribbean?fields=subregion
        @GET("/v2/region/{region}?fields=subregion")
        void getSubregionByName(@Path(value = "region") String region, Callback<List<Country>> callback);

        @GET("/v2/subregion/{subregion}?fields=name")
        void getCountriesByNameRegion(@Path(value = "subregion") String region, Callback<List<Country>> callback);

        //https://restcountries.eu/rest/v2/name/aruba
        @GET("/v2/name/{name}")
        void getCountryInfo(@Path(value = "name") String name, Callback<List<Country>> callback);

    }

    private static void initialize() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        apiInterface = restAdapter.create(ApiInterface.class);
    }


    public static void getAllInformation(Callback<List<Country>> callback) {

        apiInterface.getAllInformation(callback);



    } public static void getRegions(Callback<List<Country>> callback) {

        apiInterface.getRegions(callback);

    }

    public static void getSubregionByName(String region, Callback<List<Country>> callback) {

        apiInterface.getSubregionByName(region, callback);
    }

    public static void getCountriesByNameRegion(String subregion, Callback<List<Country>> callback) {

        apiInterface.getCountriesByNameRegion(subregion, callback);

    }

    public static void getCountryInfo(String country, Callback<List<Country>> callback) {

        apiInterface.getCountryInfo(country, callback);
    }


}
