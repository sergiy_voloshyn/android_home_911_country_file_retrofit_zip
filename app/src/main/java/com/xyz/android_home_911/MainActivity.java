package com.xyz.android_home_911;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static android.Manifest.*;

/*

1. Используя сервис https://restcountries.eu/ создать приложение "Каталог стран".
Приложение будет схоже с приложением из предыдущего задания, но есть и не мало отличий.
Приложение должно уметь показывать список частей света (regions) частей частей света (sub regions) и,
 собственно, стран. Каждый список должен быть на отдельном экране (fragment или activity, выбираете вы).
  Должна быть возможность просмотра информации по стране (столица, население, языки и пр. 5-7 пунктов).
  Важно: данные с сервера должны загружаться только один раз - при первом запуске приложения,
  для последующих запусков использовать локальный кэш (данные можно сохранить в internal storage в json формате).
1.1* Аналогичен предыдущему заданию. Добавить возможность фильтрации результатов (стран)
по названию, языку и столицам. Должно выглядеть так: пользователь в текстовое поле вводит текст,
если этот текст содержится в названии страны или ее столицы - она остается в списке, если нет - исчезает.
1.2** Добавить иконки для каждой страны https://www.gosquared.com/resources/flag-icons/.
Для этого использовать фолдер assets, в него поместить архив с иконками (32×32 будет вполне достаточно).
 При первом старте приложения все иконки необходимо скопировать на sd карту,
 на экране деталей страны добавить соответствующую иконку.


*/
public class MainActivity extends AppCompatActivity implements
        RegionElementClickListener, SubregionElementClickListener, CountriesElementClickListener,
        OnDataPass {

    public List<Country> listCountriesInfo;

    RegionFragment regionsFragment;
    SubregionFragment subregionsFragment;
    CountriesFragment countriesFragment;
    CountryInfoFragment countryInfoFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        FileInputStream fis = null;
        ObjectInputStream inputStream = null;

        boolean fileFound = false;

        try {
            fis = openFileInput("info.json");
            inputStream = new ObjectInputStream(fis);
            listCountriesInfo = (List<Country>) inputStream.readObject();


        } catch (Exception ex) {
            ex.printStackTrace();
            new AlertDialog.Builder(MainActivity.this)
                    .setTitle("Alert")
                    .setMessage(ex.toString())
                    .setCancelable(true)
                    .show();
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                    fileFound = true;
                }
                if (inputStream != null) inputStream.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        if (fileFound) {
            regionsFragment = new RegionFragment();
            subregionsFragment = new SubregionFragment();
            countriesFragment = new CountriesFragment();
            countryInfoFragment = new CountryInfoFragment();


            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.base_container, regionsFragment)
                    .addToBackStack(null)
                    .commit();

        }

        if (!fileFound) {
            Retrofit.getAllInformation(new Callback<List<Country>>() {
                @Override
                public void success(List<Country> countries, Response response) {
                    FileOutputStream fos = null;
                    ObjectOutputStream outputStream = null;

                    try {
                        fos = openFileOutput("info.json", Context.MODE_PRIVATE);
                        outputStream = new ObjectOutputStream(fos);
                        outputStream.writeObject(countries);

                        listCountriesInfo = countries;
                        regionsFragment = new RegionFragment();
                        subregionsFragment = new SubregionFragment();
                        countriesFragment = new CountriesFragment();
                        countryInfoFragment = new CountryInfoFragment();


                        getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.base_container, regionsFragment)
                                .addToBackStack(null)
                                .commit();

                    } catch (Exception ex) {
                        ex.printStackTrace();
                        new AlertDialog.Builder(MainActivity.this)
                                .setTitle("Alert")
                                .setMessage(ex.toString())
                                .setCancelable(true)
                                .show();
                    } finally {
                        try {
                            if (fos != null) fos.close();
                            if (outputStream != null) outputStream.close();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {

                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle("Alert")
                            .setMessage(error.toString())
                            .setCancelable(true)
                            .show();
                }
            });

        }


        if (ActivityCompat.checkSelfPermission(this, permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED) {
            unzipFlags();

        } else {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

        }


    }

    @Override
    public void onRegionItemClick(String item) {

        subregionsFragment.setSubregion(item);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.base_container, subregionsFragment)
                .addToBackStack(null)
                .commit();

    }

    @Override
    public void onSubregionItemClick(String item) {

        countriesFragment.setSubregion(item);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.base_container, countriesFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onCountriesItemClick(String item) {
        countryInfoFragment.setCountry(item);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.base_container, countryInfoFragment)
                .addToBackStack(null)
                .commit();

    }


    @Override
    public List<Country> getData() {
        if (listCountriesInfo != null) return this.listCountriesInfo;
        return null;
    }

    public boolean unpackZip(String path, String zipname) {
        InputStream is = null;
        ZipInputStream zis = null;


        try {
            AssetManager assetFiles = getAssets();

            String[] files = assetFiles.list("");

            for (int i = 0; i < files.length; i++) {
                if (files[i].toString().equalsIgnoreCase(zipname)) {
                    is = assetFiles.open(zipname);

                }

                String filename;
                //is = new FileInputStream( zipname);
                zis = new ZipInputStream(new BufferedInputStream(is));
                ZipEntry mZipEntry;
                byte[] buffer = new byte[1024];
                int count;

                while ((mZipEntry = zis.getNextEntry()) != null) {

                    filename = mZipEntry.getName();

                    if (mZipEntry.isDirectory()) {
                        File fmd = new File(path + filename);
                        fmd.mkdirs();
                        continue;
                    }

                    FileOutputStream fout = new FileOutputStream(path + filename);

                    String resStr = filename.substring(0, filename.indexOf('.')).toLowerCase();

                    for (int i1 = 0; i1 < listCountriesInfo.size(); i1++) {
                        String tmp = listCountriesInfo.get(i1).name.toLowerCase();

                        if (tmp.equals(resStr)) {
                            Country countryElement = listCountriesInfo.get(i1);
                            countryElement.flagImageURL = path + filename;
                            listCountriesInfo.set(i1, countryElement);
                            break;

                        }
                    }

                    while ((count = zis.read(buffer)) != -1) {
                        fout.write(buffer, 0, count);
                    }

                    fout.close();
                    zis.closeEntry();
                    //Toast.makeText(getApplicationContext(), "Success "+filename, Toast.LENGTH_SHORT).show();
                }
            }

            zis.close();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        //if we get permission
        if (requestCode == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.v("log", "Permission: " + permissions[0] + "was " + grantResults[0]);
            //  workWithExternalStorage();
            unzipFlags();

        }
    }

    public void unzipFlags() {


        String zipFilePath = Environment.getExternalStorageDirectory()
                .getAbsolutePath() + "/flags/";
        File directory = new File(zipFilePath);
        directory.mkdirs();

        unpackZip(zipFilePath, "64.zip");
    }

}

