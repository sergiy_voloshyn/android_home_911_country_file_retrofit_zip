package com.xyz.android_home_911;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SubregionFragment extends BaseFragment {

    String subregion;


    public void setSubregion(String subregion) {
        this.subregion = subregion;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_list, container, false);
        ButterKnife.bind(this, view);

        List<Country> countries = onDataPass.getData();

        HashSet<String> newSet = new HashSet<String>();

        for (int i = 0; i < countries.size(); i++) {
            String tempRegion = countries.get(i).region;
            String tempNewSubregion = countries.get(i).subregion;

            if ((!tempRegion.equals("")) & (tempRegion.equals(subregion))) {
                newSet.add(tempNewSubregion);
            }

        }

        setListData(new ArrayList<String>(newSet));
        listAdapter = new ListAdapter(getContext(), dataList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        listView.setLayoutManager(layoutManager);
        listView.setAdapter(listAdapter);

        listAdapter.setOnListClick(new ListAdapter.OnListClick() {
            @Override
            public void onItemClick(int position) {
                String item = listAdapter.getItem(position);

                subregionElementClickListener.onSubregionItemClick(item);
            }
        });

        return view;
    }

    @Override
    public BaseFragment newInstance() {
        return new SubregionFragment();
    }

    @Override
    public void setListData(List<String> dataList) {
        this.dataList = dataList;
    }

}
