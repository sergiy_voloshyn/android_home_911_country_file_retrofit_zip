package com.xyz.android_home_911;

import java.io.Serializable;

public class Country implements Serializable{

    public String name;
    public String region;
    public String subregion;
    public String capital;
    public String numericCode;
    public currency[] currencies;
    public language[] languages;
    public String flagImageURL;


    class currency implements Serializable{
        public String code;
        public String name;
        public String symbol;

    }

    class language implements Serializable{
        public String iso639_1;
        public String iso639_2;
        public String name;
        public String nativeName;
    }

}
